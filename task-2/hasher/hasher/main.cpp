/*
Description: This is example of console application

Options :
-h, --help				   Displays this help.
-s, --scan <filepath>      Find files hash in database, return 'Blocked' if
						   found or 'No threat detected' if not found
-l, --lookup <hash>        Find hash in database, works like --scan
-g, --generate <filepath>  Write file hash to console
-f, --folder <folderpath>  Scann all file in folder
*/
#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QTextStream>
#include <QDebug>
#include <QString>
#include <QStringList>
#include "hashmaker.h"
#include "HashDatabase.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	QCoreApplication::setApplicationName("AWESOM-O");
	QCommandLineParser parser;

	parser.setApplicationDescription("Description: This is example of console application");
	parser.addHelpOption();

	QCommandLineOption showScan(
		QStringList() << "s" << "scan",
		"Find files hash in database, return 'Blocked' if found or 'No threat detected' if not found",
		"filepath");
	parser.addOption(showScan);

	QCommandLineOption showLookup(
		QStringList() << "l" << "lookup",
		"Find hash in database, works like --scan",
		"hash");
	parser.addOption(showLookup);

	QCommandLineOption showGenerate(
		QStringList() << "g" << "generate",
		"Write file hash to console",
		"filepath");
	parser.addOption(showGenerate);

	QCommandLineOption showFolder(
		QStringList() << "f" << "folder",
		"Scann all file in folder",
		"folderpath");
	parser.addOption(showFolder);

	parser.process(a);

	const QStringList args = parser.optionNames();
	if (args.size() < 1) {
		qDebug() << "Error: Must specify an argument.";
		parser.showHelp(1);
	}
	try {
		hashmaker hasher;
		HashDatabase db("C:/sqlite/hashes.db");

		//--s 
		if (parser.isSet(showScan))
		{
			QString scanOpt = parser.value(showScan);

			QList<QString> HashesToScann;
			HashesToScann.append(hasher.gethash(scanOpt, QCryptographicHash::Algorithm::Md5).toHex());
			HashesToScann.append(hasher.gethash(scanOpt, QCryptographicHash::Algorithm::Sha1).toHex());
			HashesToScann.append(hasher.gethash(scanOpt, QCryptographicHash::Algorithm::Sha256).toHex());

			if(db.lookupByHashes(HashesToScann))
			{
				qDebug() << "Result: Blocked";
			}
			else
			{
				qDebug() << "Result: No threat detected";
			}
		}
		//--l
		if (parser.isSet(showLookup))
		{
			QString loodupOpt = parser.value(showLookup);

			if (db.lookupByHash(loodupOpt))
			{
				qDebug() << "Result: Blocked";
			}
			else
			{
				qDebug() << "Result: No threat detected";
			}
		}
		//--g
		if (parser.isSet(showGenerate))
		{
			QString generateOpt = parser.value(showGenerate);
			try {
				hasher.printFileHashes(generateOpt);
			}
			catch (std::exception e) {
				qDebug() << e.what();
			}
		}
		//--f
		if (parser.isSet(showFolder))
		{
			QString folderOpt = parser.value(showFolder);

			QDir recoredDir(folderOpt);
			QStringList allFiles = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::Files);
			for (auto file : allFiles) {
				QString path = QDir(folderOpt).filePath(file); //handle user input for path i.e. missing backslash at the end of 
				qDebug() << file << ": ";
				QList<QString> HashesToScann;
				HashesToScann.append(hasher.gethash(path, QCryptographicHash::Algorithm::Md5).toHex());
				HashesToScann.append(hasher.gethash(path, QCryptographicHash::Algorithm::Sha1).toHex());
				HashesToScann.append(hasher.gethash(path, QCryptographicHash::Algorithm::Sha256).toHex());

				if (db.lookupByHashes(HashesToScann))
				{
					qDebug() << "\tResult: Blocked";
				}
				else
				{
					qDebug() << "\tResult: No threat detected";
				}
			}
			//qDebug() << folderOpt;
		}
	}
	catch (std::invalid_argument e)
	{
		qDebug() << e.what();
	}

	//return a.exec();
	return 0;
}
