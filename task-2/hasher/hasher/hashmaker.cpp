#include "hashmaker.h"



hashmaker::hashmaker()
{
}


hashmaker::~hashmaker()
{
}

QByteArray hashmaker::gethash(QString filepath, QCryptographicHash::Algorithm algorithm)
{

	QFile f(filepath);
	if (!f.open(QFile::ReadOnly))
	{
		throw std::invalid_argument("File not accessable");
	}
	
	QCryptographicHash hash(algorithm);
	if (hash.addData(&f)) {
		return hash.result();
	}
}

void hashmaker::printFileHashes(QString filepath)
{
	qDebug() << "MD5: " << gethash(filepath, QCryptographicHash::Algorithm::Md5).toHex();
	qDebug() << "SHA1: " << gethash(filepath, QCryptographicHash::Algorithm::Sha1).toHex();
	qDebug() << "SHA256: " << gethash(filepath, QCryptographicHash::Algorithm::Sha256).toHex();

}
