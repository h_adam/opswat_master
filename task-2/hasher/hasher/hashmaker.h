#pragma once
#include <qstring.h>
#include <qfile.h>
#include <QCryptographicHash>
#include <qdebug.h>
class hashmaker
{
public:
	hashmaker();
	~hashmaker();
	QByteArray gethash(QString file, QCryptographicHash::Algorithm algorithm);
	void printFileHashes(QString fie);
};

