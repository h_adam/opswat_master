#pragma once
#include <QtSql>
#include <qlist.h>
class HashDatabase
{
	QSqlDatabase _db;
public:
	HashDatabase(QString dbFile);
	~HashDatabase();
	bool lookupByHashes(QList<QString> hashes);
	bool lookupByHash(QString hash);
};

