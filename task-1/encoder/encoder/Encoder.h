#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <map>

class Encoder
{
private:
	std::map<std::string, std::string> table_encode;
	std::map<std::string, std::string> table_decode;
public:
	Encoder();
	~Encoder();
	bool load(std::string path);
	int strLength(const char str[]);
	char* easytolower(char* in);
	std::string encode_str(const char* input);
	std::string decode_str(const char* input);
	std::string encode_file(const char* input);
	std::string decode_file(const char* input);
};

