#include <iostream>
#include <string.h>
#include "Encoder.h"



int main(){
	Encoder parser;
	std::string file_path;

	/*ask for the json file*/
	std::cout << "Where is the dictionary JSON file?" << std::endl;
	std::getline(std::cin, file_path);/*getline because spaces and whitespaces*/
	while ( !parser.load(file_path)){
		/*file not found or no access for it*/
		std::cout << "ERROR: Invalid coding table" << std::endl << "Where is the dictionary JSON file?" << std::endl;
		std::getline(std::cin, file_path);
	}

	/*ask user to write action*/
	std::string action;
	std::cout << "Waiting for user input" << std::endl;
	std::getline(std::cin, action); 
	const char* c_action = action.c_str();
	
	while (strncmp("EXIT", c_action, 4) != 0){
		c_action = action.c_str(); /*refresh variable. Spent time here ~1,5h*/
		if (strncmp("ENCODE_FILE ", c_action, 12) == 0)
		{
			std::cout << parser.encode_file((const char*)parser.easytolower((char*)action.erase(0, 12).c_str())) << std::endl;
		} 
		else if (strncmp("DECODE_FILE ", c_action, 12) == 0)
		{
			std::cout << parser.decode_file((const char*)parser.easytolower((char*)action.erase(0, 12).c_str())) << std::endl;
		}
		else if (strncmp("DECODE ", c_action, 7) == 0)
		{
			std::cout << parser.decode_str((const char*)parser.easytolower((char*)action.erase(0, 7).c_str())) << std::endl;
			/*user called decode, and pass remain input to encoder*/
		}
		else if (strncmp(c_action, "ENCODE ", 7) == 0)
		{
			std::cout << parser.encode_str((const char*)parser.easytolower((char*)action.erase(0, 7).c_str())) << std::endl;
			/*user called encode, and pass remain input to encoder*/

		}
		else
		{
			std::cout << "Invalid Action" << std::endl;
			/*wrong input command */
		}
		/*ask for the next input action*/
		std::cout << "Waiting for user input" << std::endl;
		std::getline(std::cin, action);
		const char* c_action = action.c_str();

	}
	std::cout << "KTHXBYE" << std::endl;

	system("pause");
	return 0;
}