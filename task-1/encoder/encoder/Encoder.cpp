#include "Encoder.h"

int Encoder::strLength(const char str[]) {
	/*count characters in array*/
	int i = 0;
	for (i = 0; str[i] != '\0'; ++i);
	return i;
}

char* Encoder::easytolower(char* in){
	/*convert to lowercase*/
	for (int i = 0; i < strLength(in); i++)
	{
		if (in[i] <= 'Z' && in[i] >= 'A')
			in[i] -= ('Z' - 'z');
	}
	return in;
}

Encoder::Encoder()
{
}


Encoder::~Encoder()
{
}

bool Encoder::load(std::string path)
{
	/*Loading json file from path*/
	rapidjson::Document json;

	std::ifstream infile(path);
	if ( ! infile.good()) return false;
	/*unable to open file */

	/*read json as string, than parse it*/
	std::stringstream buff;
	buff << infile.rdbuf();
	if (json.Parse(buff.str().c_str()).HasParseError())
	{
		infile.close();
		return false;
		/*error in json file: syntax or otthers*/
	}

	/*fill the maps with dictionary rules*/
	for (rapidjson::Value::ConstMemberIterator itr = json.MemberBegin();
		itr != json.MemberEnd(); ++itr)
	{
		table_encode[itr->name.GetString()] = itr->value.GetString();
		table_decode[itr->value.GetString()] = itr->name.GetString();
	}
	infile.close();
	return true;
}

std::string Encoder::encode_str(const char* input)
{
	/*encoding a string char by char*/
	std::stringstream return_stream;
	for (int i = 0; i < strLength(input); i++)
	{
		/*to be sure*/
		std::stringstream tmp;
		tmp.str("");
		tmp << input[i];

		if (0 != table_encode.count(tmp.str())){
			/*has rule for char*/
			return_stream << table_encode.find(tmp.str())->second;
		}
		else{
			/*not has rule for char*/
			return "INVALID INPUT";
		}
	}
	return return_stream.str();
}

std::string Encoder::decode_str(const char* input){
	/*decodeing line*/
	std::stringstream return_stream;
	std::string s_input(input);

	int change = 0; /*prevents inf loop*/
	int s_length = strLength(s_input.c_str());/*how many input char left to decode*/

	while (s_length  != 0)
	{
		/*while input not empty*/
		change = 0;
		for (std::map<std::string, std::string>::iterator it = table_decode.begin(); it != table_decode.end(); ++it)
		{
			/*iterating though rules to find the next match*/
			if (s_input.find(it->first) == 0){
				/*if has match push the decode value to stream*/
				change++;
				s_input.erase(0, strLength(it->first.c_str())); /*erease input begin*/
				return_stream << it->second;
				s_length -= strLength(it->first.c_str());
			}
		}
		if (change == 0)
		{
			/*no rule match, its an error*/
			return "INVALID INPUT";
		}
	}
	return return_stream.str();
}

std::string Encoder::encode_file(const char* input)
{
	std::stringstream input_file("");
	std::stringstream output_file("");
	std::stringstream return_stream;

	int space_idx = (int)(std::strchr(input, ' ') - input); /*magic, return index of first space*/
	if (space_idx > 0)/*negative if not conains, and 0 if first char is space, that is bullshit*/
	{
		for (int i = 0; i < space_idx; i++) input_file << input[i];	/*geting the file names*/
		for (int i = space_idx + 1; i < strLength(input); i++) output_file << input[i];

		std::ifstream infile(input_file.str());
		if (!infile.good()) return "INVALID INPUT FILE"; /*input file is invalid*/

		std::ofstream outfile(output_file.str());
		if (!outfile.good()) return "INVALID OUTPUT FILE"; /*output file is invalid*/

		for (std::string line; std::getline(infile, line);)
		{
			/*loop thought input file line by line*/
			std::string encoded_row = encode_str(line.c_str());
			if (encoded_row != "INVALID INPUT")
			{
				/*converting one line*/
				/*std::cout << encoded_row << std::endl;*/
				outfile << encoded_row.c_str() << std::endl;
			}
			else
			{
				outfile.close();
				infile.close();
				return "INVALID INPUT";
				/*failed to convert row*/
			}
		}
		outfile.close();
		infile.close();
		//std::cout << input_file.str() << std::endl << output_file.str() << std::endl;
	}
	else
	{
		return "INVALID INPUT FILE";
	}

	return return_stream.str();
}

std::string Encoder::decode_file(const char* input)
{
	std::stringstream input_file("");
	std::stringstream output_file("");
	std::stringstream return_stream;

	int space_idx = (int)(std::strchr(input, ' ') - input); /*magic, return index of first space*/
	if (space_idx > 0)/*negative if not conains, and 0 if first char is space, that is bullshit*/
	{
		for (int i = 0; i < space_idx; i++) input_file << input[i];	/*geting the file names*/
		for (int i = space_idx + 1; i < strLength(input); i++) output_file << input[i];

		std::ifstream infile(input_file.str());
		if (!infile.good()) return "INVALID INPUT FILE"; /*input file is invalid*/

		std::ofstream outfile(output_file.str());
		if (!outfile.good()) return "INVALID OUTPUT FILE"; /*output file is invalid*/

		for (std::string line; std::getline(infile, line);)
		{
			/*loop thought input file line by line*/
			std::string decoded_row = decode_str(line.c_str());
			if (decoded_row != "INVALID INPUT")
			{
				/*converting one line*/
				/*std::cout << encoded_row << std::endl;*/
				outfile << decoded_row.c_str() << std::endl;
			}
			else
			{
				outfile.close();
				infile.close();
				return "INVALID INPUT";
				/*failed to convert row*/
			}
		}
		outfile.close();
		infile.close();
		//std::cout << input_file.str() << std::endl << output_file.str() << std::endl;
	}
	else
	{
		return "INVALID INPUT FILE";
	}

	return return_stream.str();
}