/*
Description: This is example of console application

Options :
-h, --help				   Displays this help.
-s, --scan <filepath>	  Find files hash in database, return 'Blocked' if
						   found or 'No threat detected' if not found
-l, --lookup <hash>		Find hash in database, works like --scan
-g, --generate <filepath>  Write file hash to console
-f, --folder <folderpath>  Scann all file in folder
*/
/*QSqlQuery query;
query.prepare("INSERT INTO hashes(md5, sha1, sha256) values(:1,:2,:3)");
query.bindValue(":1","b1180ebaa7a6a5249f32f427806b9727");
query.bindValue(":2","01ecf8a09aaf9758865ad0142c8bd0e2edeebbd0");
query.bindValue(":3","7f07883a02a699bfc4fc6f55e94076722d798cd1f30575eb7d9c81c58cf2852f");
qDebug()<<query.exec()<<endl;
*/
#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QTextStream>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QHash>
#include "HashMaker.h"

void prettyPrintHashes(QHash<QString, QString> hash)
{
	 foreach(QString i, hash.keys()) qDebug() << i << ":" <<hash[i];
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	QCoreApplication::setApplicationName("AWESOM-O");
	QCommandLineParser parser;

	parser.setApplicationDescription("Description: This is example of console application");
	parser.addHelpOption();

	QCommandLineOption showScan(
		QStringList() << "s" << "scan",
		"Find files hash in database, return 'Blocked' if found or 'No threat detected' if not found",
		"filepath");
	parser.addOption(showScan);

	QCommandLineOption showLookup(
		QStringList() << "l" << "lookup",
		"Find hash in database, works like --scan",
		"hash");
	parser.addOption(showLookup);

	QCommandLineOption showGenerate(
		QStringList() << "g" << "generate",
		"Write file hash to console",
		"filepath");
	parser.addOption(showGenerate);

	QCommandLineOption showFolder(
		QStringList() << "f" << "folder",
		"Scann all file in folder",
		"folderpath");
	parser.addOption(showFolder);

	parser.process(a);

	const QStringList args = parser.optionNames();
	if (args.size() < 1) {
		qDebug() << "Error: Must specify an argument.";
		parser.showHelp(1);
	}
	try {
		HashMaker hasher("/home/adam/hashes.db");

		//--s 
		if (parser.isSet(showScan))
		{
			QString scanOpt = parser.value(showScan);

			if(hasher.checkFile(scanOpt))
			{
				qDebug() << "Result: Blocked";
			}
			else
			{
				qDebug() << "Result: No threat detected";
			}
		}
		//--l
		if (parser.isSet(showLookup))
		{
			QString loodupOpt = parser.value(showLookup);

			if (hasher.checkHash(loodupOpt))
			{
				qDebug() << "Result: Blocked";
			}
			else
			{
				qDebug() << "Result: No threat detected";
			}
		}
		//--g
		if (parser.isSet(showGenerate))
		{
			QString generateOpt = parser.value(showGenerate);
			prettyPrintHashes(hasher.generateHashFromFile(generateOpt));
		}
		//--f
		if (parser.isSet(showFolder))
		{
			QString folderOpt = parser.value(showFolder);

			QHashIterator<QString, bool> iter(hasher.checkFolder(folderOpt));
			while(iter.hasNext())
			{
				iter.next();
				if(iter.value())
				{
					qDebug() << iter.key() << " : " << "Result: Blocked";
				}
				else
				{
					qDebug() << iter.key() << " : " << "Result: No threat detected";
				}
			}
		}
	}
	catch (std::invalid_argument e)
	{
		qDebug() << e.what();
	}

	//return a.exec();
	return 0;
}
