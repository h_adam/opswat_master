#include "HashDatabase.h"



HashDatabase::HashDatabase(QString dbFile)
{
	_db = QSqlDatabase::addDatabase("QSQLITE");
	_db.setDatabaseName(dbFile);
	if(!_db.open()) throw std::invalid_argument("DB file not accessable");
}


HashDatabase::~HashDatabase()
{
	_db.close();
}

bool HashDatabase::lookupByHashes(QList<QString> hashes)
{
	QSqlQuery query;
	query.prepare("SELECT COUNT(id) as `0` FROM hashes WHERE md5 = (:md5) AND sha1 = (:sha1) AND sha256 = (:sha256)");
	query.bindValue(":md5", hashes[0]);
	query.bindValue(":sha1", hashes[1]);
	query.bindValue(":sha256", hashes[2]);
	query.exec();
	query.first();

	if (query.value(0).toString().toInt() >= 1)
		return true;
	else
		return false;

}

bool HashDatabase::lookupByHash(QString hash)
{
	QSqlQuery query;
	query.prepare("SELECT COUNT(id) as `0` FROM hashes WHERE md5 = (:hash) OR sha1 = (:hash) OR sha256 = (:hash)");
	query.bindValue(":hash", hash);
	query.exec();
	query.first();

	if (query.value(0).toString().toInt() >= 1) 
		return true;
	else
		return false;

}
