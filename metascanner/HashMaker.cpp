#include "HashMaker.h"
#include <QHash>

HashMaker::HashMaker(QString dbfile)
{
	db = new HashDatabase(dbfile);
}


HashMaker::~HashMaker()
{
	delete db;
}

//make hash from file
QByteArray HashMaker::gethash(QString filepath, QCryptographicHash::Algorithm algorithm)
{

	QFile f(filepath);
	if (!f.open(QFile::ReadOnly))
	{
		throw std::invalid_argument("File not accessable");
	}
	
	QCryptographicHash hash(algorithm);
	if (hash.addData(&f)) {
		return hash.result();
	}
}

bool HashMaker::checkFile(QString file){
	QList<QString> HashesToScann;
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Md5).toHex());
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Sha1).toHex());
	HashesToScann.append(gethash(file, QCryptographicHash::Algorithm::Sha256).toHex());
	return db->lookupByHashes(HashesToScann);
}

bool HashMaker::checkHash(QString hashStr)
{
	return db->lookupByHash(hashStr);
}
QHash<QString, bool> HashMaker::checkFolder(QString folder2scann)
{
	QHash<QString, bool> hash2Return;

	QDir recoredDir(folder2scann);
	QStringList allFiles = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::Files);
	
	for (auto file : allFiles) {
		QString path = QDir(folder2scann).filePath(file); //handle user input for path i.e. missing backslash at the end of 

		QList<QString> HashesToScann;
		HashesToScann.append(gethash(path, QCryptographicHash::Algorithm::Md5).toHex());
		HashesToScann.append(gethash(path, QCryptographicHash::Algorithm::Sha1).toHex());
		HashesToScann.append(gethash(path, QCryptographicHash::Algorithm::Sha256).toHex());
		hash2Return.insert(path, db->lookupByHashes(HashesToScann));
	}
	return hash2Return;
}

QHash<QString, QString> HashMaker::generateHashFromFile(QString file){
	QHash<QString, QString> hash2Return;
	hash2Return.insert("md5", gethash(file, QCryptographicHash::Algorithm::Md5).toHex());
	hash2Return.insert("sha1", gethash(file, QCryptographicHash::Algorithm::Sha1).toHex());
	hash2Return.insert("sha256", gethash(file, QCryptographicHash::Algorithm::Sha256).toHex());
	return hash2Return;
}
