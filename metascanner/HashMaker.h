#pragma once
#include <qstring.h>
#include <qfile.h>
#include <QCryptographicHash>
#include "HashDatabase.h"
#include <qdebug.h>
class HashMaker
{
public:
	HashMaker(QString dbfile);
	~HashMaker();
	QByteArray gethash(QString file, QCryptographicHash::Algorithm algorithm);
	bool checkFile(QString file);
	bool checkHash(QString hashStr);
	QHash<QString, bool>  checkFolder(QString folder2scann);
	QHash<QString, QString> generateHashFromFile(QString file);
protected:
	HashDatabase *db;
};

